import React, {useEffect} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import LottieView from 'lottie-react-native';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 3000);
  }, []);
  return (
    <View style={styles.background}>
      <LottieView
        source={require('../../assets/splash.json')}
        autoPlay
        loop={false}
        speed={0.5}
        onAnimationFinish={() => {
          console.log('Animation Finished!');
          // navigation.replace('Home');
        }}
      />
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'white',
  },
});
