import React from 'react';
import {View, Text, StyleSheet, ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import {wine} from '../assets';

const SuccessPage = props => {
  return (
    <ImageBackground source={wine} style={styles.back}>
      <View style={styles.sukses}>
        <Text style={styles.berhasil}>Payment is successful!</Text>
        <Text style={styles.terimakasih}>Thank you!</Text>
      </View>
    </ImageBackground>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SuccessPage);

const styles = StyleSheet.create({
  sukses: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  berhasil: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  back: {
    flex: 1,
  },
  terimakasih: {
    color: 'white',
  },
});
