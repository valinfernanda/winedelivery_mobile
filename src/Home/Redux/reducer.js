const InitialState = {
  nomor: '',
  // kodeproduk: '',
  respon: {},
};

export const WineReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_WINE':
      return {
        ...state,
        nomor: action.nomor,
      };
    case 'SET_WINE_DETAIL':
      return {
        ...state,
        // kodeproduk: action.kode,
        respon: action.respon,
      };

    default:
      return state;
  }
};
