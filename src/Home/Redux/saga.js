import {all, takeLatest, put, call} from 'redux-saga/effects';
import axios from 'axios';
import {navigate} from '../../utils/Nav';

import {setLoading} from '../../Store/GlobalAction';

function* SagaWine(data) {
  try {
    yield put(setLoading(true));

    const Response = yield axios.get(
      // `https://sandbox.centralbiller.co.id/product/sub?product_code=PLN-PREPAID`,
      `https://zax5j10412.execute-api.ap-southeast-1.amazonaws.com/dev/api/product/list?page=1`,
    );

    console.log(Response, 'hasil response untuk coba');

    yield put({
      type: 'SET_WINE',
      nomor: Response.data.value,
    });
    yield put(navigate('WineDetail', {}));
  } catch (error) {
    console.log(error);
  } finally {
    yield put(setLoading(false));
  }
}

function* SagaWineDetail(id) {
  console.log(id, 'ini isi data yo');
  try {
    const response = yield axios.get(
      `https://zax5j10412.execute-api.ap-southeast-1.amazonaws.com/dev/api/product/${id.kodeproduk}`,
    );

    console.log(response, 'hasil response wine detail');

    yield put({
      type: 'SET_WINE_DETAIL',
      // kode: data.kodeproduk,
      // nomor: data.nomorpelanggan,
      respon: response.data.value,
    });

    yield put(navigate('WineDetail', {}));
  } catch (error) {
    console.log(error);
  }
}

export function* SagasWine() {
  yield takeLatest('GET_WINE', SagaWine);
  yield takeLatest('GET_WINE_DETAIL', SagaWineDetail);
}
