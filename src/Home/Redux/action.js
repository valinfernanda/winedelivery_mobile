export const ActionWine = nomor => {
  return {
    type: 'GET_WINE',
    nomor,
  };
};

export const ActionWineDetail = kodeproduk => {
  return {
    type: 'GET_WINE_DETAIL',
    kodeproduk,
  };
};
