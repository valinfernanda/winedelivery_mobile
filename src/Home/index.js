import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ImageBackground,
  Image,
  Alert,
} from 'react-native';

import {ActionWine, ActionWineDetail} from './Redux/action';
import {connect} from 'react-redux';
import {TouchableOpacity} from 'react-native-gesture-handler';

import Gap from '../components/Gap';
import Button from '../components/Button';
import {bookmark, wine2} from '../assets';
import FastImage from 'react-native-fast-image';
import Header from '../components/Header';

const Home = props => {
  const [kode, setKode] = useState('');

  useEffect(() => {
    props.ActionWine();
  }, []);

  console.log(props.nama, 'ini propsnya name');

  const showAlert = () => {
    Alert.alert(`{props.nama} is added to cart!`);
  };

  const showAlert1 = () => {
    Alert.alert(`{props.nama} is bookmarked!`);
  };

  return (
    <>
      <Header title="Wine Delivery" />
      <ImageBackground source={wine2} style={styles.back}>
        <View>
          {/* <Text style={styles.tulisan}>Wine Delivery</Text> */}

          <FlatList
            showsVerticalScrollIndicator={false}
            // data={props.listToken}
            data={props.listWine}
            renderItem={({item, index}) => {
              return (
                <View>
                  <TouchableOpacity
                    key={index}
                    style={styles.token}
                    onPress={() => {
                      setKode(item.id);
                      props.ActionWineDetail(kode);
                    }}>
                    <View>
                      <View style={styles.kotak}>
                        <View style={styles.tempatbookmark}>
                          <TouchableOpacity onPress={showAlert1}>
                            {/* <Text style={styles.textbutton}>zz</Text> */}
                            <Image style={styles.bookmark} source={bookmark} />
                          </TouchableOpacity>
                        </View>
                        <View style={styles.pilihtoken}>
                          <Text style={styles.tulisansatu}>{item.name}</Text>
                          <Gap height={10} />
                          <FastImage
                            source={{
                              uri: item.image,
                            }}
                            style={{height: 300, width: 150, borderRadius: 10}}
                          />
                          <Text style={styles.tulisan2}>{item.producer}</Text>

                          <Text style={styles.tulisan2}>
                            {item.region}, {item.country}
                          </Text>

                          <Text style={styles.tulisan}>$ {item.price}</Text>

                          <TouchableOpacity
                            onPress={showAlert}
                            style={styles.button}>
                            <Text style={styles.textbutton}>Add to Cart</Text>
                          </TouchableOpacity>
                          <Gap height={10} />
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              );
            }}
          />

          <Gap height={20} />

          <Gap height={30} />
        </View>
      </ImageBackground>
    </>
  );
};

const mapStateToProps = state => ({
  listWine: state.WineReducer.nomor.products,
  nama: state.WineReducer.respon.name,
});

const mapDispatchToProps = {
  ActionWine,
  ActionWineDetail,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
  back: {
    flex: 1,
    // backgroundColor: '#ecf0f1',
  },
  pilihtoken: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  kotak: {
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 10,
  },
  textbutton: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  pasca: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  button: {
    // backgroundColor: '#4ba37b',
    backgroundColor: 'black',

    width: 200,
    height: 30,
    borderRadius: 50,
    alignItems: 'center',
    marginTop: 10,
    fontSize: 20,
  },
  tempatbookmark: {
    justifyContent: 'flex-end',
    flex: 1,
    flexDirection: 'row',
    // backgroundColor: 'blue',
  },
  bookmark: {
    width: 40,
    height: 50,
  },
  token: {
    paddingHorizontal: 30,
  },
  tulisansatu: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  background: {
    flex: 1,
  },
  tulisandua: {
    fontSize: 20,
  },
  tulisantiga: {
    fontSize: 15,
  },
  tulisan: {
    fontSize: 30,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: 10,
  },
  tulisan2: {
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignSelf: 'center',
    // paddingTop: 10,
  },
});
