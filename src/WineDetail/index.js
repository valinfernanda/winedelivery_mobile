import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import Button from '../components/Button';
import Gap from '../components/Gap';
import FastImage from 'react-native-fast-image';
import Header from '../components/Header';

import Loading from '../components/Loading';

const WineDetail = props => {
  return (
    <>
      <Header title="Wine Details" />
      <View>
        <View style={styles.container}>
          <View style={styles.detailPageContainer}>
            <Text style={styles.jenis}>{props.produk.name}</Text>
            <Gap height={15} />
            <FastImage
              source={{
                uri: props.produk.image,
              }}
              style={{height: 300, width: 150, borderRadius: 10}}
            />
            <Gap height={15} />

            <Text style={styles.bold1}>{props.produk.grapeVarieties}</Text>
            <Text>{props.noRef}</Text>
            <Gap height={1} />
            <Text style={styles.bold}>Producer: {props.produk.producer}</Text>
            <Text style={styles.bold}>Alcohol: {props.produk.alcohol}</Text>
            <Text style={styles.bold}>
              Origin: {props.produk.region}, {props.produk.country}
            </Text>
            <Text style={styles.bold}>
              Bottle Size: {props.produk.bottleSize}
            </Text>
            <Text style={styles.bold}>Quantity Left: {props.produk.qty}</Text>

            <Gap height={7} />

            <Text style={styles.bold}>Price :</Text>
            <Text style={styles.harga}>$ {props.produk.price}</Text>
          </View>
        </View>
        <Gap height={600} />
        <Button
          title="Checkout"
          onPress={() => {
            props.navigation.navigate('SuccessPage');
          }}
        />
      </View>
    </>
  );
};

const mapStateToProps = state => ({
  produk: state.WineReducer.respon,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(WineDetail);

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: '#ffffff',
    flexDirection: 'row',

    justifyContent: 'center',
  },
  detailPageContainer: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',

    top: 20,
  },
  productName: {
    fontWeight: 'bold',
    fontSize: 25,
  },
  bold: {
    fontWeight: 'bold',
  },
  bold1: {
    fontWeight: 'bold',
    fontSize: 17,
  },
  jenis: {
    fontWeight: 'bold',
    fontSize: 23,
  },
  harga: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
