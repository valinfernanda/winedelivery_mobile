import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from '../Splash';
import Home from '../Home';
import SuccessPage from '../SuccessPage';
import WineDetail from '../WineDetail';

const Stack = createStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SuccessPage"
        component={SuccessPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="WineDetail"
        component={WineDetail}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
