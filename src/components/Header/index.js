import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Gap from '../Gap';

const Header = ({title}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{title}</Text>
      <Gap width={24} />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingVertical: 30,
    backgroundColor: 'black',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  text: {
    flex: 1,
    textAlign: 'center',
    fontSize: 25,
    fontFamily: 'Nunito-SemiBold',
    color: 'white',

    textTransform: 'capitalize',
  },
});
