import React from 'react';
import {View, ActivityIndicator} from 'react-native';

export default function Loading() {
  return (
    // <View style={[Layouting().centered, Layouting().flex]}>
    <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
      <ActivityIndicator size={12} color="blue" />
    </View>
  );
}
