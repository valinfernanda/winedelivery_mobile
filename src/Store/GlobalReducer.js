import {SET_LOADING} from './GlobalAction';
const InitialState = {
  language: 'Indonesia',
  theme: 'dark',
  isLoading: false,
};

export const GlobalReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'CHANGE_LANGUAGE':
      return {
        ...state,
        language: 'English',
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      return state;
  }
};
