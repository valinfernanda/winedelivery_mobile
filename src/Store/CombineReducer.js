import {combineReducers} from 'redux';
import {GlobalReducer} from './GlobalReducer';
import {WineReducer} from '../../src/Home/Redux/reducer';

export const AllReducer = combineReducers({
  GlobalReducer,
  WineReducer,
});
