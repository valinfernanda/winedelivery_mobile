import {all} from 'redux-saga/effects';
import {SagasWine} from '../../src/Home/Redux/saga';

export function* SagaWatcher() {
  yield all([SagasWine()]);
}
